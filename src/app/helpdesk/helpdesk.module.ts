import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FaqComponent } from './faq/faq.component';
import { RaiseTicketComponent } from './raise-ticket/raise-ticket.component';
import { ConsHelpdeskComponent } from './cons-helpdesk/cons-helpdesk.component';
import { ViewTicketsComponent } from './view-tickets/view-tickets.component';
import { AppRoutingModule } from '../app-routing.module';
import { AngularMaterialModule } from '../angular-material/angular-material.module';
import { ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    FaqComponent,
    RaiseTicketComponent,
    ConsHelpdeskComponent,
    ViewTicketsComponent
  ],
  imports: [
    CommonModule,
    AppRoutingModule,
    AngularMaterialModule,
    ReactiveFormsModule
  ]
})
export class HelpdeskModule { }
