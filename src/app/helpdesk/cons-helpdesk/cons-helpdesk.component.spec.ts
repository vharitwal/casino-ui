import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsHelpdeskComponent } from './cons-helpdesk.component';

describe('ConsHelpdeskComponent', () => {
  let component: ConsHelpdeskComponent;
  let fixture: ComponentFixture<ConsHelpdeskComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConsHelpdeskComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ConsHelpdeskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
