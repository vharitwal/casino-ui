import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ConsHelpdeskComponent } from './helpdesk/cons-helpdesk/cons-helpdesk.component';
import { FaqComponent } from './helpdesk/faq/faq.component';
import { RaiseTicketComponent } from './helpdesk/raise-ticket/raise-ticket.component';
import { ViewTicketsComponent } from './helpdesk/view-tickets/view-tickets.component';
import { GameListComponent } from './player/game-list/game-list.component';
import { LoginComponent } from './player/login/login.component';
import { RegistrationComponent } from './player/registration/registration.component';

const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent},
  { path: 'registration', component: RegistrationComponent},
  { path: 'view-tickets', component: ViewTicketsComponent},
  { path: 'helpdesk', component: ConsHelpdeskComponent},
  { path: 'game-list', component: GameListComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
