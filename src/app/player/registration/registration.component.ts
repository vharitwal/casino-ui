import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

  hide1 = true;
  hide2 = true;

  formGroup = this.fb.group({
    firstName: ['', [Validators.required]],
    lastName: ['', [Validators.required]],
    username: ['', [Validators.required, Validators.pattern("^[a-zA-Z\\d_]+$")]],
    email: ['', [Validators.required, Validators.email]],
    mob: ['', [Validators.required, Validators.pattern("^[0-9]{10}$")]],
    dob: ['', [Validators.required, this.ageValidator()]],
    password: ['', [Validators.required, Validators.pattern("^(?=.*[A-Za-z])(?=.*\\d)(?=.*[@$!%*#?&])[A-Za-z\\d@$!%*#?&]{8,}$")]],
    rePassword: ['', [Validators.required, Validators.pattern("^(?=.*[A-Za-z])(?=.*\\d)(?=.*[@$!%*#?&])[A-Za-z\\d@$!%*#?&]{8,}$")]],
    cb: [false, [Validators.requiredTrue]]
  }, {
    validators: this.controlValuesAreEqual('password', 'rePassword')
  });

  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
  }

  private ageValidator(): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      var ageDifMs = Date.now() - control.value;
      var ageDate = new Date(ageDifMs);
      const age = Math.abs(ageDate.getUTCFullYear() - 1970);
      return age >= 18 ? null : {underage: true};
    };
  }

  private controlValuesAreEqual(controlNameA: string, controlNameB: string): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      const formGroup = control as FormGroup
      const valueOfControlA = formGroup.get(controlNameA)?.value
      const valueOfControlB = formGroup.get(controlNameB)?.value

      if (valueOfControlA === valueOfControlB) {
        return null
      } else {
        return { valuesDoNotMatch: true }
      }

    }
  }

}
