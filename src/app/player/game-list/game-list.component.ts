import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-game-list',
  templateUrl: './game-list.component.html',
  styleUrls: ['./game-list.component.css']
})
export class GameListComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  games: {
    name: string,
    credits: number
    img: string
  }[] = [
    {
      name: "Blackjack",
      credits: 10,
      img: "assets/pics/game-list/blackjack.jpeg"
    },
    {
      name: "Roulette",
      credits: 12,
      img: "assets/pics/game-list/roulette.jpg"
    },
    {
      name: "Craps",
      credits: 8,
      img: "assets/pics/game-list/craps.jpg"
    },
    {
      name: "Poker",
      credits: 15,
      img: "assets/pics/game-list/poker.jpg"
    },
    {
      name: "Baccarat",
      credits: 20,
      img: "assets/pics/game-list/baccarat.jpeg"
    },
    {
      name: "Video Poker",
      credits: 20,
      img: "assets/pics/game-list/videopoker.jpg"
    }
  ]

}
